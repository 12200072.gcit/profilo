import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";

import chatify from "../../Assets/Projects/1.jpeg";
import Luyang from "../../Assets/Projects/2.jpeg";
import blood from "../../Assets/Projects/2.png";
import blood1 from "../../Assets/Projects/1.png";
import Lhakor from "../../Assets/Projects/3.jpg";

import mess from "../../Assets/Projects/3.png";
import mess1 from "../../Assets/Projects/4.png";


function Projects() {
  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          My Recent <strong className="purple">Works </strong>
        </h1>
        <p style={{ color: "white" }}>
          Here are a few projects I've worked on recently.
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={chatify}
              isBlog={false}
              title="DrukCalcultor"
              description="DrukCalculator is designed for Bhutanese people and supports both Dzongkha and English languages"
              ghLink="https://gitlab.com/12200072.gcit/drukcalculator"
              demoLink=""
            />
          </Col>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={Luyang}
              isBlog={false}
              title="Luyang"
              description="Luyang is a platform that digitally stores Bhutanese song lyrics to preserve the culture for the future."
              ghLink="https://gitlab.com/12200072.gcit/group13"
              demoLink="https://www.youtube.com/watch?v=vrP-CREABAk"
            />
          </Col>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={blood}
              isBlog={false}
              title="Blood Donation"
              description="Blood Donation is a platform where users can both request blood and donate it to those in need."
              ghLink="https://gitlab.com/12200072.gcit/itw202/-/blob/main/bloodApp.7z?ref_type=heads"
              demoLink="https://www.youtube.com/watch?v=iH5nffy33Y0"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={blood1}
              isBlog={false}
              title="Pamphlet"
              description="Pamphlet is a platform where users can write and post their content"
              ghLink=""
              demoLink=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={Lhakor}
              isBlog={false}
              title="Lhakor"
              description="Lhakor is an app where users can book a taxi anytime."
              ghLink=""
              demoLink=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={mess}
              isBlog={false}
              title="Mess Procement System"
              description="The Mess Procurement System is a platform for managing the procurement of supplies for a mess or dining facility."
              ghLink="https://gitlab.com/2023its1g7.gcit/mess-procurement-system"
              demoLink=""
            />
          </Col>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={mess1}
              isBlog={false}
              title="Second Hand"
              description=""
              ghLink="https://gitlab.com/sonamyangki/second-hand-shop"
              demoLink="https://react-project-mu-lyart.vercel.app/home"
            />
          </Col>




       
        </Row>
      
      </Container>
    </Container>
  );
}

export default Projects;
